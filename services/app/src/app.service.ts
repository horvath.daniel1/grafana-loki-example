import { Injectable } from '@nestjs/common';
import { Span } from '@nitedani/logging-nestjs';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  throwError(): string {
    throw new Error('Hello error');
  }

  @Span()
  process2() {
    throw new Error('Hello error');
    return 'Almafa';
  }

  @Span()
  process(): any {
    let arr = ['2', '2'];
    for (let index = 0; index < 24; index++) {
      arr = [...arr, ...arr];
    }

    const almafa = this.process2();

    return arr.reduce((acc, curr) => acc + curr);
  }
}
