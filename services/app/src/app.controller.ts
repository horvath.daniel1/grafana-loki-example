import { BadRequestException, Controller, Get, Logger } from '@nestjs/common';
import { Span } from '@nitedani/logging-nestjs';
import axios from 'axios';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private appService: AppService) {}
  private logger = new Logger(AppController.name);
  @Get('/hello')
  getHello(): string {
    this.logger.log('TESZT ALMAFA123');
    setTimeout(() => {
      throw new Error('kiskutya');
    }, 1000);
    return 'Hello';
  }

  @Get('/hello1')
  getHello1() {
    return axios.get('https://httpbin.org/status/200').then((res) => res.data);
  }

  @Get('/span')
  spanTest(): string {
    this.logger.log('IN SPAN TESZT');
    const processed = this.appService.process();
    return processed;
  }

  @Span()
  @Get('/hello2')
  getHello2(): any {
    let arr = ['2', '2'];
    for (let index = 0; index < 24; index++) {
      arr = [...arr, ...arr];
    }

    return arr.reduce((acc, curr) => acc + curr);
  }

  @Get('/400')
  async get400() {
    throw new BadRequestException('This is a 400 error');
  }

  @Span()
  @Get('/error')
  throwError(): string {
    throw new Error('Hello error');
  }
}
