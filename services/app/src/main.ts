import { logging } from '@nitedani/logging-nestjs';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

const options = {
  service_name: 'test_service',
  console: true,
  loki: {
    host: 'http://loki:3100',
  },
  tempo: {
    host: 'http://tempo:14268',
  },
};
console.log('asdasd');

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bufferLogs: true,
  });

  logging(app as any, options);

  await app.listen(4000);
}
bootstrap();

setInterval(() => {
  //throw new Error('This is a test background task error');
}, 5000);
